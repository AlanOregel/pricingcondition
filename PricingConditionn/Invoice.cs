﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PricingConditionn
{
    class Invoice
    {

        public void ProcessJEGeneration(string formUID)
        {

            string id;

            try
            {

               

                SAPbouiCOM.DBDataSource oDatatable;
               // SAPbobsCOM.JournalEntries oJounal;

                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbobsCOM.Recordset oRecSet1 = Global.Recordset();
                SAPbobsCOM.Recordset oRecSet2 = Global.Recordset();
                SAPbobsCOM.Recordset oRecInsert = Global.Recordset();

                SAPbouiCOM.Form coForm = Global.SBOApp.Forms.Item(formUID);

                id = DateTime.Now.ToString("yyyyMMddhhmmss");

                oDatatable = coForm.DataSources.DBDataSources.Item("OINV");
                string stDocEntry = oDatatable.GetValue("DocEntry", 0);

                oRecSet.DoQuery(Global.ChooseQuery(9, stDocEntry));

                if (oRecSet.RecordCount == 0)
                {
                    return;
                }

                oRecSet.DoQuery(Global.ChooseQuery(7, stDocEntry));
                oRecSet.MoveFirst();

                if (oRecSet.RecordCount > 0)
                {

                    for (int i = 0; i < oRecSet.RecordCount; i++)
                    {
                   

                        //Global.oCompany.StartTransaction();

                        //oJounal = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries);

                        //oJounal.ReferenceDate = oRecSet.Fields.Item("DocDate").Value;
                        //oJounal.DueDate = oRecSet.Fields.Item("DocDate").Value;
                        //oJounal.TaxDate = oRecSet.Fields.Item("TaxDate").Value;
                        //oJounal.Memo = "Accrual posting for AR Invoice: " + oRecSet.Fields.Item("DocNum").Value;
                        //oJounal.Reference = oRecSet.Fields.Item("DistNumber").Value;
                        //oJounal.Reference2 = oRecSet.Fields.Item("ItemCode").Value;
                        //oJounal.Reference3 = stDocEntry;
                        //oJounal.UserFields.Fields.Item("U_B1S_PCBASETYPE").Value = "13";


                        oRecSet1.DoQuery(Global.ChooseQuery(1, oRecSet.Fields.Item("ItemCode").Value.ToString(), oRecSet.Fields.Item("DocDate").Value.ToString("yyyy-MM-dd")));

                        for (int j = 0; j < oRecSet1.RecordCount; j++)
                        {
                            oRecSet2.DoQuery(Global.ChooseQuery(8, oRecSet1.Fields.Item("Code").Value));

                            for (int k = 0; k < oRecSet2.RecordCount; k++)
                            {

                                try
                                {
                                    oRecInsert.DoQuery("INSERT INTO [dbo].[B1S_PC_INVJE]  VALUES  " +
                                                "('" + id + "'" +
                                                ",'" + stDocEntry + "'" +
                                                ",'" + oRecSet.Fields.Item("DocNum").Value + "'" +
                                                ",'" + oRecSet.Fields.Item("ItemCode").Value + "'" +
                                                ",'" + oRecSet.Fields.Item("DocDate").Value.ToString("yyyy-MM-dd") + "'" +
                                                ",'" + oRecSet.Fields.Item("TaxDate").Value.ToString("yyyy-MM-dd") + "'" +
                                                ",'" + oRecSet.Fields.Item("DistNumber").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("Code").Value + "'" +
                                                ",'" + oRecSet.Fields.Item("DocDate").Value.ToString("yyyy-MM-dd") + "'" +
                                                ",'" + oRecSet2.Fields.Item("LineId").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("U_PCItemCode").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("U_PCItemDesc").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("U_Amt").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("U_ProfitCtr").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("U_DRGL").Value + "'" +
                                                ",'" + oRecSet2.Fields.Item("U_CRGL").Value + "'" +
                                                ",'W'" +
                                                ",NULL)");
                                }
                                catch (Exception)
                                {
                                    SAPbobsCOM.Recordset ors = Global.Recordset();
                                    ors.DoQuery($"DELETE FROM B1S_PC_INVJE WHERE Id = '{id}'");
                                }
                             

                                

                                //oJounal.Lines.AccountCode = oRecSet2.Fields.Item("U_DRGL").Value;
                                //oJounal.Lines.Debit = oRecSet2.Fields.Item("U_Amt").Value;
                                //oJounal.Lines.LineMemo = "Accrual posting for AR Invoice: " + oRecSet.Fields.Item("DocNum").Value;
                                //oJounal.Lines.CostingCode = oRecSet2.Fields.Item("U_ProfitCtr").Value;
                                //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCTYPE").Value = "ITM";
                                //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCCODE").Value = oRecSet2.Fields.Item("Code").Value;
                                //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCLINE").Value = oRecSet2.Fields.Item("LineId").Value;

                                //oJounal.Lines.Add();

                                
                                //oJounal.Lines.AccountCode = oRecSet2.Fields.Item("U_CRGL").Value;
                                //oJounal.Lines.Credit = oRecSet2.Fields.Item("U_Amt").Value;
                                //oJounal.Lines.LineMemo = "Accrual posting for AR Invoice: " + oRecSet.Fields.Item("DocNum").Value;
                                //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCTYPE").Value = "ITM";
                                //oJounal.Lines.CostingCode = oRecSet2.Fields.Item("U_ProfitCtr").Value;
                                //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCCODE").Value = oRecSet2.Fields.Item("Code").Value;
                                //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCLINE").Value = oRecSet2.Fields.Item("LineId").Value;

                                //oJounal.Lines.Add();

                                oRecSet2.MoveNext();
                            }

                            oRecSet1.MoveNext();
                        }

                        Global.SetMessage("Journal Entry request added: Id " + id, Global.MsgType.Success);

                        //if (oJounal.Lines.Count > 0)
                        //{

                        //    if (oJounal.Add() == 0)
                        //    {
                        //        string stTransId = Global.oCompany.GetNewObjectKey();
                        //        Global.SetMessage("Journal Entry added. TransId: " + stTransId, Global.MsgType.Success);
                        //        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        //    }
                        //    else
                        //    {

                        //        Global.oCompany.GetLastError(out int error, out string errorMsg);

                        //        Global.SetMessage("Error. " + errorMsg, Global.MsgType.Error);

                        //        if (Global.oCompany.InTransaction)
                        //        {
                        //            Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        //        }
                        //    }
                        //}

                        oRecSet.MoveNext();
                    }


                }

            }
            catch (Exception ex)
            {

                Global.SetMessage("ValidateJEGeneration: " + ex.Message, Global.MsgType.Error);
                //if (Global.oCompany.InTransaction)
                //{
                //    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                //}
            }
        }

    }
}
