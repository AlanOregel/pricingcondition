﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PricingConditionn
{
    class Be1
    {

        #region Variables

        public CatchingEvents oCatchingEvents;

        #endregion

        #region Connection

        public Be1()
        {
            SetApplication();
            GetDirectory();
            SetFilters();
            SetConnectionContext();
            InitializeEvents();

        }
        public void SetApplication()
        {
            try
            {
                SAPbouiCOM.SboGuiApi SboGuiApi = null;
                string sConnectionString = null;

                SboGuiApi = new SAPbouiCOM.SboGuiApi();
                sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));
                SboGuiApi.Connect(sConnectionString);
                Global.SBOApp = SboGuiApi.GetApplication(-1);
            }
            catch (Exception e)
            {
                MessageBox.Show("SetApplication" + e.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        public void GetDirectory()
        {
            try
            {
                Global.csDirectory = Environment.CurrentDirectory + @"\";
            }
            catch (Exception e)
            {
                MessageBox.Show("GetDirectory: " + e.Message);
            }

        }

        private void SetConnectionContext()
        {
            try
            {
                Global.oCompany = Global.SBOApp.Company.GetDICompany();
              
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetConnectionContext: " + ex.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        private void SetFilters()
        {

            try
            {
                EventFilter lofilter;
                EventFilters lofilters = new SAPbouiCOM.EventFilters();

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);


                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
                lofilter.AddEx("139");
                lofilter.AddEx("UDO_FT_B1S_PC_HEADER");
                lofilter.AddEx("UDO_FT_B1S_PCVPO_H");


                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
                lofilter.AddEx("139");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
                lofilter.AddEx("133");
                lofilter.AddEx("179");
                
                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
                lofilter.AddEx("139");
                lofilter.AddEx("UDO_FT_B1S_PC_HEADER");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
                lofilter.AddEx("UDO_FT_B1S_PC_HEADER");               

                Global.SBOApp.SetFilter(lofilters);


            }
            catch (Exception e)
            {
                Global.SetMessage("SetFilters: " + e.Message, Global.MsgType.Error);
            }

        }

        #endregion

        #region Events

        public void InitializeEvents()
        {
            oCatchingEvents = new CatchingEvents();
            Global.SBOApp.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBOApp_AppEvent);
            Global.SBOApp.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBOApp_FormDataEvent);
            Global.SBOApp.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBOApp_MenuEvent);
            Global.SBOApp.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBOApp_ItemEvent);


        }

        private void SBOApp_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {

            switch (EventType)
            {
                case BoAppEventTypes.aet_ShutDown:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_ServerTerminition:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_CompanyChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_LanguageChanged:
                    System.Windows.Forms.Application.Restart();
                    break;
            }
        }


        private void SBOApp_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinnesObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oCatchingEvents.SBOApplication_FormDataEvent(ref BusinnesObjectInfo, out BubbleEvent);
        }


        private void SBOApp_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oCatchingEvents.SBOApplication_MenuEvent(ref pVal, out BubbleEvent);

        }

        private void SBOApp_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oCatchingEvents.SBOApplication_ItemEvent(FormUID, ref pVal, out BubbleEvent);
        }

        #endregion


    }
}
