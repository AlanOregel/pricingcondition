--------------------------------------------------------------------------------------------------------------------------------
--PRICING CONDITION
	Declare @Aux1 as int	
	DECLARE @AuxDateFrom AS DATE
	DECLARE @AuxDateTill AS DATE
	DECLARE @ItemCode AS VARCHAR(50)
	DECLARE @Aux AS INT
	DECLARE @Aux2 AS INT
	DECLARE @AuxEntry AS INT
	DECLARE @PricCodeAux AS VARCHAR(50)
	DECLARE @PricCode AS VARCHAR(50)
	DECLARE @VPOAux AS VARCHAR(50)

		IF @object_type = 'B1S_PC_HEADER' and @transaction_type in('A','U')
		BEGIN


			SELECT @AuxDateFrom = T0.U_EffDate, @AuxDateTill = T0.U_ValidTill, @ItemCode = T0.U_ItemCode, @AuxEntry = T0.DocEntry
			FROM [@B1S_PC_HEADER] T0 
			WHERE T0.Code = @list_of_cols_val_tab_del
	
			SELECT @Aux = COUNT(T0.Code)
			FROM [@B1S_PC_HEADER] T0 
			WHERE T0.U_ItemCode = @ItemCode AND (T0.U_EffDate <= @AuxDateTill AND T0.U_ValidTill >= @AuxDateFrom) AND T0.DocEntry <> @AuxEntry

			IF @Aux > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'Valid period overlaps with another register.'
			END
		--------------------------------------------------------------------------------------------------------------------------------

		END 

		IF @object_type = 'B1S_PC_HEADER' and @transaction_type in('A','U')
		BEGIN

	
			SELECT @Aux = COUNT(T0.Code)
			FROM [@B1S_PC_HEADER] T0 
			WHERE T0.Code = @list_of_cols_val_tab_del AND (T0.U_EffDate IS NULL OR T0.U_ValidTill IS NULL)

			IF @Aux > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'Please enter a period for the registrer.'
			END

		END 

		IF @object_type = 'B1S_PC_HEADER' and @transaction_type in('U')
		BEGIN
			
			SELECT @AuxEntry = T0.DocEntry FROM [@B1S_PC_HEADER] T0  WHERE T0.DocEntry = @list_of_cols_val_tab_del


			SELECT TOP 1 @PricCodeAux = TX.Code FROM 
			(SELECT TOP 2  T0.Code, T0.UpdateDate, T0.UpdateTime from [@AB1S_PC_HEADER] T0 WHERE T0.DocEntry = @AuxEntry ORDER BY T0.UpdateDate, T0.UpdateTime DESC) TX
			ORDER BY  TX.UpdateDate, TX.UpdateTime

			SELECT @PricCode = T0.Code 
			FROM [@B1S_PC_HEADER] T0 
			WHERE T0.DocEntry = @AuxEntry
			
			IF @PricCode <> @PricCodeAux
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'The Code for the registrer can not be updated.'
			END

		END


		IF @object_type = 'B1S_PCVPO_H' and @transaction_type in('A','U')
		BEGIN

			SELECT @VPOAux = ISNULL(T0.U_VPO,'') FROM [@B1S_PCVPO_H] T0 WHERE T0.DocEntry = @list_of_cols_val_tab_del

			SELECT @Aux = COUNT(T0.DocEntry) FROM [@B1S_PCVPO_H] T0 WHERE T0.DocEntry <> @list_of_cols_val_tab_del AND T0.U_VPO = @VPOAux

			IF @VPOAux = ''
			BEGIN
				SELECT @error=8001
				 SET @error_message= 'VPO No. must be filled.'
			END

			IF @Aux > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'VPO No. has been already used.'
			END

		END


		IF @object_type = 'B1S_PC_HEADER' and @transaction_type in('A','U')
		BEGIN


			SELECT @Aux = COUNT(T0.Code)
			FROM [@B1S_PC_ROW] T0  LEFT OUTER JOIN OACT T1 ON T0.U_DRGL = T1.FormatCode 
			WHERE  isnull(T0.U_DRGL,'') != '' AND (T1.Postable = 'N' OR T1.FrozenFor = 'Y')	
					AND T0.Code = @list_of_cols_val_tab_del
	
			SELECT  @Aux2 = COUNT(T0.Code)
			FROM [@B1S_PC_ROW] T0  LEFT OUTER JOIN OACT T1 ON T0.U_CRGL = T1.FormatCode 
			WHERE isnull(T0.U_CRGL,'') != '' AND (T1.Postable = 'N' OR T1.FrozenFor = 'Y')
				 AND T0.Code = @list_of_cols_val_tab_del

	
	
			IF @Aux > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'One of the Debit accounts is not valid.'
			END


			IF @Aux2 > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'One of the Credit accounts is not valid.'
			END
		--------------------------------------------------------------------------------------------------------------------------------

		END 

		
		IF @object_type = 'B1S_PC_HEADER' and @transaction_type in('A','U')
		BEGIN


			SELECT @Aux = COUNT(T0.Code)
			FROM [@B1S_PC_ROW] T0  LEFT OUTER JOIN OACT T1 ON T0.U_DRGL = T1.FormatCode 
			WHERE  isnull(T0.U_DRGL,'') != '' AND (ISNULL(T1.FormatCode,'') = '')	
					AND T0.Code = @list_of_cols_val_tab_del
	
			SELECT  @Aux2 = COUNT(T0.Code)
			FROM [@B1S_PC_ROW] T0  LEFT OUTER JOIN OACT T1 ON T0.U_CRGL = T1.FormatCode 
			WHERE isnull(T0.U_CRGL,'') != '' AND (ISNULL(T1.FormatCode,'') = '')	
				 AND T0.Code = @list_of_cols_val_tab_del

	
	
			IF @Aux > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'One debit account does not exist.'
			END


			IF @Aux2 > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'One credit account does not exist..'
			END
		END 


		IF @object_type = '14' and @transaction_type in('A')
		BEGIN

			SELECT @Aux = COUNT(T0.DocEntry)  
			FROM RIN1 T0 INNER JOIN B1S_PC_INVJE T1 ON T0.BaseEntry = T1.DocEntry
			WHERE T0.DocEntry = @list_of_cols_val_tab_del AND T1.PC_JE_Status = 'W' AND T1.PC_JE_TransId IS NULL


			IF @Aux > 0
			BEGIN
				 SELECT @error=8001
				 SET @error_message= 'Base Document has a Pricing Condition Journal Entry pending, please wait until it is created.'
			END

		END
	
	--------------------------------------------------------------------------------------------------------------------------------
