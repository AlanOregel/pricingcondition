﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingConditionn
{
    class PricingCondition
    {
        private SAPbouiCOM.Form coForm;

        public void AddFormItems(String FormUID)
        {
            try
            {

                SAPbouiCOM.MenuCreationParams oCreationPackage;
                oCreationPackage = Global.SBOApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                oCreationPackage.Checked = false;
                oCreationPackage.Enabled = true;
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "PCDuplicate";
                oCreationPackage.String = "Duplicate";

                coForm = Global.SBOApp.Forms.Item(FormUID);
                coForm.Menu.AddEx(oCreationPackage);

                oCreationPackage.Checked = false;


            }
            catch (Exception e)
            {
                Global.SetMessage("AddFormItems. " + e.Message, Global.MsgType.Error);
            }
        }

        public void DuplicatePricingCondition()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                SAPbouiCOM.DBDataSource loDBDataSource;

                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE)
                {

                    loDBDataSource = coForm.DataSources.DBDataSources.Item("@B1S_PC_HEADER");
                    string stCode = loDBDataSource.GetValue("Code", 0);

                    if (stCode != "")
                    {
                        FillNewPC(stCode);
                    }
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("DuplicatePricingCondition. " + e.Message, Global.MsgType.Error);
            }
        }

        public void FillNewPC(string stCode)
        {
            try
            {
                SAPbouiCOM.EditText oEdit;
                SAPbouiCOM.Matrix oMatrix;

                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                Global.SBOApp.ActivateMenuItem("1282");
                coForm.Freeze(true);
                oRecSet.DoQuery("Select T0.U_ItemCode, T0.U_ProfitCtr, T0.U_NettPrice, T1.U_PCItemCode, " +
                                "T1.U_PCItemDesc, T1.U_Sign, T1.U_DiscAmt, T1.U_DRGL, T1.U_CRGL, T1.U_Print, T1.U_Amt " +
                                "FROM[@B1S_PC_HEADER] T0 LEFT OUTER JOIN[@B1S_PC_ROW] T1 ON T0.Code = T1.Code " +
                                "WHERE T0.Code = '" + stCode + "'");
                oRecSet.MoveFirst();

                oEdit = coForm.Items.Item("13_U_E").Specific;
                oEdit.Value = oRecSet.Fields.Item("U_ItemCode").Value;

                oEdit = coForm.Items.Item("16_U_E").Specific;
                oEdit.Value = oRecSet.Fields.Item("U_NettPrice").Value.ToString();

                oEdit = coForm.Items.Item("17_U_E").Specific;
                oEdit.Value = oRecSet.Fields.Item("U_ProfitCtr").Value;

                oMatrix = coForm.Items.Item("0_U_G").Specific;

                SAPbouiCOM.DBDataSource loDBDataSource = coForm.DataSources.DBDataSources.Item("@B1S_PC_ROW");

                for (int i = 1; i <= oRecSet.RecordCount; i++)
                {

                    loDBDataSource = coForm.DataSources.DBDataSources.Item("@B1S_PC_ROW");
                    loDBDataSource.SetValue("LineId", loDBDataSource.Size - 1, i.ToString());
                    loDBDataSource.SetValue("U_PCItemCode", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_PCItemCode").Value);
                    loDBDataSource.SetValue("U_PCItemDesc", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_PCItemDesc").Value);
                    loDBDataSource.SetValue("U_Sign", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_Sign").Value);
                    loDBDataSource.SetValue("U_DiscAmt", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_DiscAmt").Value);
                    loDBDataSource.SetValue("U_DRGL", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_DRGL").Value);
                    loDBDataSource.SetValue("U_CRGL", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_CRGL").Value);
                    loDBDataSource.SetValue("U_Print", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_Print").Value);
                    loDBDataSource.SetValue("U_Amt", loDBDataSource.Size - 1, oRecSet.Fields.Item("U_Amt").Value);


                    if (i != oRecSet.RecordCount)
                    {
                        loDBDataSource.InsertRecord(loDBDataSource.Size);
                    }

                    oMatrix.LoadFromDataSource();
                    oRecSet.MoveNext();
                }

                coForm.Freeze(false);
            }
            catch (Exception e)
            {
                Global.SetMessage("FillNewPC. " + e.Message, Global.MsgType.Error);
                coForm.Freeze(false);
            }
        }

        public void CalculateNetSales()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                coForm.Freeze(true);
                SAPbouiCOM.DBDataSource loDBDataSource, loDBDataSourceL;
                double discount;
                string printFlag, drgl, crgl, sign;



                SAPbouiCOM.Matrix oMatrix = coForm.Items.Item("0_U_G").Specific;

                loDBDataSource = coForm.DataSources.DBDataSources.Item("@B1S_PC_HEADER");
                string stPrice = (loDBDataSource.GetValue("U_ListPrice", 0));
                double dbPrice = Convert.ToDouble(stPrice);

                loDBDataSource.SetValue("U_NettPrice", 0, stPrice);

                oMatrix.FlushToDataSource();

                loDBDataSourceL = coForm.DataSources.DBDataSources.Item("@B1S_PC_ROW");

                for (int i = 0; i < loDBDataSourceL.Size; i++)
                {

                    printFlag = loDBDataSourceL.GetValue("U_Print", i);
                    drgl = loDBDataSourceL.GetValue("U_DRGL", i);
                    crgl = loDBDataSourceL.GetValue("U_CRGL", i);
                    sign = loDBDataSourceL.GetValue("U_Sign", i);
                    if (sign.Replace(" ", "") != "" && drgl.Replace(" ", "") == "" && crgl.Replace(" ", "") == "" && printFlag.Replace(" ", "") == "N")
                    {
                        discount = Convert.ToDouble(loDBDataSourceL.GetValue("U_Sign", i) + loDBDataSourceL.GetValue("U_DiscAmt", i));
                        dbPrice = dbPrice + discount;
                    }
                }


                if (dbPrice >= 0)
                {
                    loDBDataSource.SetValue("U_NettPrice", 0, dbPrice.ToString());
                }
                else
                {
                    loDBDataSource.SetValue("U_NettPrice", 0, "0.00");
                }

                coForm.Freeze(false);

            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage($"CalculateNetSales. {e.Message}", Global.MsgType.Error);
            }
        }



        public void ReNoLineId()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                coForm.Freeze(true);

                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    SAPbouiCOM.DBDataSource loDBDataSourceL;

                    SAPbouiCOM.Matrix oMatrix = coForm.Items.Item("0_U_G").Specific;
                    oMatrix.FlushToDataSource();

                    loDBDataSourceL = coForm.DataSources.DBDataSources.Item("@B1S_PC_ROW");

                    for (int i = 0; i < loDBDataSourceL.Size; i++)
                    {
                        loDBDataSourceL.SetValue("LineId", i, Convert.ToString(i + 1));
                    }

                    oMatrix.LoadFromDataSource();

                }

                coForm.Freeze(false);

            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage($"CalculateNetSales. {e.Message}", Global.MsgType.Error);
            }
        }


        public bool DisableRestoreVPO()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                if (coForm.TypeEx == "UDO_FT_B1S_PCVPO_H")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage($"CalculateNetSales. {e.Message}", Global.MsgType.Error);
                return true;
            }
        }
    }
}
