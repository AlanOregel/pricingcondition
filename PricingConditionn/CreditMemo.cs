﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingConditionn
{
    class CreditMemo
    {
        public void ProcessReverseJE(string formUID)
        {
            string id = DateTime.Now.ToString("yyyyMMddhhmmss");

            try
            {
                SAPbouiCOM.DBDataSource oDatatable;
                //SAPbobsCOM.JournalEntries oJounal;

                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbobsCOM.Recordset oRecSet2 = Global.Recordset();
                SAPbobsCOM.Recordset oRecSet3 = Global.Recordset();

                SAPbouiCOM.Form coForm = Global.SBOApp.Forms.Item(formUID);

                oDatatable = coForm.DataSources.DBDataSources.Item("ORIN");
                string stDocEntry = oDatatable.GetValue("DocEntry", 0);
                string stDocNum = oDatatable.GetValue("DocNum", 0);

                oRecSet3.DoQuery(Global.ChooseQuery(10, stDocEntry));
                oRecSet3.MoveFirst();
                

                if (oRecSet3.RecordCount > 0)
                {
                    for (int k = 0; k < oRecSet3.RecordCount; k++)
                    {

                        string lstTransId = oRecSet3.Fields.Item(0).Value.ToString();

                        oRecSet.DoQuery("INSERT INTO [dbo].[B1S_PC_RINJE] " +
                                         "VALUES " +
                                         "('" + id + "' " +
                                         ",'" + stDocEntry + "'" +
                                         ",'" + stDocNum + "'" +
                                         ",'" + lstTransId + "'" +
                                         ",'W'" +
                                         ",NULL)");

                        Global.SetMessage("Journal Entry request added. Id: " + id , Global.MsgType.Success);

                        //oRecSet2.DoQuery(Global.ChooseQuery(12, stDocEntry));
                        //oRecSet.DoQuery(Global.ChooseQuery(11, lstTransId));
                        //oRecSet.MoveFirst();


                        //Global.oCompany.StartTransaction();
                      //  oJounal = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oinvoic);

                        //oJounal.ReferenceDate = oRecSet2.Fields.Item("DocDate").Value;
                        //oJounal.DueDate = oRecSet2.Fields.Item("DocDate").Value;
                        //oJounal.TaxDate = oRecSet2.Fields.Item("TaxDate").Value;
                        //oJounal.Memo = "Reversal of accrual for AR CN: " + oRecSet2.Fields.Item("DocNum").Value;

                        //oJounal.Reference = oRecSet.Fields.Item("Ref1").Value;
                        //oJounal.Reference2 = oRecSet.Fields.Item("Ref2").Value;
                        //oJounal.Reference3 = oRecSet2.Fields.Item("Reference3").Value;
                        //oJounal.UserFields.Fields.Item("U_B1S_PCBASETYPE").Value = "14";

                        //for (int i = 0; i < oRecSet.RecordCount; i++)
                        //{
                        //    //oJounal.Lines.AccountCode = oRecSet.Fields.Item("Account").Value;
                        //    //oJounal.Lines.Credit = oRecSet.Fields.Item("Debit").Value;
                        //    //oJounal.Lines.Debit = oRecSet.Fields.Item("Credit").Value;
                        //    //oJounal.Lines.LineMemo = "Reversal of accrual for AR CN: " + oRecSet2.Fields.Item("DocNum").Value;
                        //    //oJounal.Lines.CostingCode = oRecSet.Fields.Item("ProfitCode").Value;
                        //    //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCTYPE").Value = "ITM";
                        //    //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCCODE").Value = oRecSet.Fields.Item("U_B1S_PCCODE").Value;
                        //    //oJounal.Lines.UserFields.Fields.Item("U_B1S_PCLINE").Value = oRecSet.Fields.Item("U_B1S_PCLINE").Value;

                        //    //oJounal.Lines.Add();

                        //    oRecSet.MoveNext();
                        //}

                        //if (oJounal.Lines.Count > 0)
                        //{

                        //    if (oJounal.Add() == 0)
                        //    {
                        //        string stTransId = Global.oCompany.GetNewObjectKey();
                        //        Global.SetMessage("Journal Entry added. TransId: " + stTransId, Global.MsgType.Success);
                        //        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        //    }
                        //    else
                        //    {

                        //        Global.oCompany.GetLastError(out int error, out string errorMsg);

                        //        Global.SetMessage("Error. " + errorMsg, Global.MsgType.Error);

                        //        if (Global.oCompany.InTransaction)
                        //        {
                        //            Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        //        }
                        //    }
                        //}

                        oRecSet3.MoveNext();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("ProcessReverseJE. " + ex.Message, Global.MsgType.Error);
            }
        }
    }
}
