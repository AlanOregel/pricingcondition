﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingConditionn
{
    class CatchingEvents
    {
        public CatchingEvents()
        {
           
        }

        public void SBOApplication_FormDataEvent(ref BusinessObjectInfo BusinnesObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {        

                if (BusinnesObjectInfo.FormTypeEx == "133")
                {
                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD & BusinnesObjectInfo.ActionSuccess)
                    {

                        Invoice oInvoice = new Invoice();
                        oInvoice.ProcessJEGeneration(BusinnesObjectInfo.FormUID);

                    }
                }

                if (BusinnesObjectInfo.FormTypeEx == "179")
                {
                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD & BusinnesObjectInfo.ActionSuccess)
                    {

                        CreditMemo oCM = new CreditMemo();
                        oCM.ProcessReverseJE(BusinnesObjectInfo.FormUID);

                    }
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("FormDataEvent" + e.Message, Global.MsgType.Error);
            }
        }


        public void SBOApplication_MenuEvent(ref MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if(pVal.BeforeAction == true & pVal.MenuUID== "PCDuplicate")
                {
                    PricingCondition oPricingCond = new PricingCondition();
                    oPricingCond.DuplicatePricingCondition();
                }

                if (pVal.BeforeAction == true & pVal.MenuUID == "1285" )
                {
                    PricingCondition oPricingCond = new PricingCondition();
                    BubbleEvent = oPricingCond.DisableRestoreVPO();
                    
                }

               

            }
            catch (Exception e)
            {
                Global.SetMessage("Menu Event:" + e.Message, Global.MsgType.Error);
            }
        }

        public void SBOApplication_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            try
            {
                BubbleEvent = true;


                if (pVal.BeforeAction == false & pVal.FormTypeEx != "")
                {
                    switch (pVal.FormTypeEx)
                    {
                        case "139":
                            FrmSalesOrderControllerAfter(FormUID, pVal);    
                            break;
                        case "UDO_FT_B1S_PC_HEADER":
                            FrmPricingConditionControllerAfter(FormUID, pVal);
                            break;

                    }
                }

                if (pVal.BeforeAction == true & pVal.FormTypeEx != "")
                {
                    switch (pVal.FormTypeEx)
                    {
                        case "UDO_FT_B1S_PC_HEADER":
                            FrmPricingConditionControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;    
                    }
                }

            }
            catch (Exception e)
            {
                BubbleEvent = false;
                Global.SetMessage("ItemEvent: " + e.Message, Global.MsgType.Error);
            }

        }


        public void FrmSalesOrderControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                SalesOrder oSales = new SalesOrder();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        oSales.AddFormItems(pVal.FormUID);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btPrcCon":
                                oSales.SetPricingConditionItems();
                                break;
                            case "1":
                              //  oSales.UpdateDiscountDetail(pVal.FormUID);
                                break;
                        }

                        break;     
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void FrmPricingConditionControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                PricingCondition oPricingCond = new PricingCondition();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        oPricingCond.AddFormItems(pVal.FormUID);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btPrcCon":
                             //   oPricingCond.SetPricingConditionItems();
                                break;
                            case "1":
                                //  oSales.UpdateDiscountDetail(pVal.FormUID);
                                break;
                        }

                        break;
                    case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS:

                        switch (pVal.ItemUID)
                        {
                            case "15_U_E":
                                  oPricingCond.CalculateNetSales();
                                break;
                            case "0_U_G":

                                oPricingCond.ReNoLineId();

                                switch (pVal.ColUID)
                                {
                                    case "C_0_5":
                                    case "C_0_4":
                                    case "C_0_6":
                                    case "C_0_7":
                                    case "C_0_8":
                                        oPricingCond.CalculateNetSales();
                                        break;
                                }

                                break;
                        }

                        break;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void FrmPricingConditionControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubleEvent)
        {
            try
            {
                BubleEvent = true;

                PricingCondition oPricingCond = new PricingCondition();

                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                                oPricingCond.CalculateNetSales();
                                BubleEvent = true;
                                break;
                            default:
                                BubleEvent = true;
                                break;
                        }

                        break;
                    default:
                        BubleEvent = true;
                        break;

                }

            }
            catch (Exception ex)
            {
                BubleEvent = false;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }
    }
}
