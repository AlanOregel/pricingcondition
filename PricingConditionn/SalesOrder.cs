﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingConditionn
{
    class SalesOrder
    {

        private SAPbouiCOM.Form coForm;

        public void AddFormItems(String FormUID)
        {
            try
            {
                SAPbouiCOM.Item loItem;
                SAPbouiCOM.Button loButton;
                String lsItemRef;
                
                coForm = Global.SBOApp.Forms.Item(FormUID);
                lsItemRef = "2";

                loItem = coForm.Items.Add("btPrcCon", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                loItem.Left = coForm.Items.Item(lsItemRef).Left + coForm.Items.Item(lsItemRef).Width + 10;
                loItem.Top = coForm.Items.Item(lsItemRef).Top;
                loItem.Width = coForm.Items.Item(lsItemRef).Width + 30;
                loItem.Height = coForm.Items.Item(lsItemRef).Height;
                loButton = loItem.Specific;
                loButton.Caption = "Pricing Condition";

            }
            catch (Exception e)
            {
                Global.SetMessage("AddFormItems. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SetPricingConditionItems()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                SAPbouiCOM.EditText oItemCode, oDocDate, oPCType, oNumAtCard;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                SAPbouiCOM.DBDataSource loDBDataSource;
                string stDocEntry, stDocStatus;
                SAPbobsCOM.Documents oDocument;

                loDBDataSource = coForm.DataSources.DBDataSources.Item("ORDR");
                stDocEntry = loDBDataSource.GetValue("DocEntry", 0);
                stDocStatus = loDBDataSource.GetValue("DocStatus", 0);


                if (coForm.Mode != SAPbouiCOM.BoFormMode.fm_OK_MODE)
                {
                    return;
                }

                if(stDocStatus != "O")
                {
                    return;
                }

                coForm.Freeze(true);

                oMatrix = coForm.Items.Item("38").Specific;
                int iniRowCount = oMatrix.RowCount;

                oDocument = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                oDocument.GetByKey(Convert.ToInt32(stDocEntry));

                
                for (int i = 1; i < iniRowCount; i++)
                {
                    oDocDate = coForm.Items.Item("10").Specific;
                    oItemCode = oMatrix.Columns.Item("1").Cells.Item(i).Specific;
                    oPCType = oMatrix.Columns.Item("U_B1S_PCTYPE").Cells.Item(i).Specific;
                 
                    oRecSet.DoQuery(Global.ChooseQuery(1, oItemCode.Value, oDocDate.Value));
                  
                    if (oRecSet.RecordCount > 0 && oPCType.Value == "")
                    {  
                        AddPClines(oRecSet.Fields.Item(0).Value, i, stDocEntry, oDocument); 
                    }
                }


                oNumAtCard = coForm.Items.Item("14").Specific;

                oRecSet.DoQuery(Global.ChooseQuery(3, oNumAtCard.Value));

                if (oRecSet.RecordCount > 0)
                {
                    AddVPOLines(stDocEntry,oRecSet.Fields.Item(0).Value.ToString(), oNumAtCard.Value, oDocument);
                }


                if (oDocument.Update() == 0)
                {
                    Global.SetMessage("Pricing Condition Complete. ", Global.MsgType.Success);
                }
                else
                {
                    Global.oCompany.GetLastError(out int errCode, out string errmsg);
                    Global.SetMessage("Update Sales Order: " + errmsg, Global.MsgType.Error);
                }

                coForm.Freeze(false);
                Global.SBOApp.ActivateMenuItem("1304");
                

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetPricingConditionsItems. " + ex.Message, Global.MsgType.Error);
                coForm.Freeze(false);
            }
        }


        private void AddPClines(string code, int lineid, string docentry, SAPbobsCOM.Documents oDocument)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbouiCOM.Matrix oMatrix;
                int j;

                oMatrix = coForm.Items.Item("38").Specific;
               
                oRecSet.DoQuery(Global.ChooseQuery(2, code));
                oRecSet.MoveFirst();

                for(int i = 0; i < oRecSet.RecordCount; i++)
                {
                    j = oMatrix.RowCount;

                    if (ValidateRecord("ITM", docentry, oRecSet.Fields.Item("Code").Value.ToString(), oRecSet.Fields.Item("LineId").Value.ToString(), lineid.ToString()))
                    {

                        oDocument.Lines.Add();
                        oDocument.Lines.ItemCode = oRecSet.Fields.Item("U_PCItemCode").Value;
                        oDocument.Lines.ItemDescription = oRecSet.Fields.Item("U_PCItemDesc").Value;
                        oDocument.Lines.Quantity = Convert.ToDouble(oRecSet.Fields.Item("U_Sign").Value + "1");
                        oDocument.Lines.UnitPrice = oRecSet.Fields.Item("U_DiscAmt").Value;
                        oDocument.Lines.Currency = Global.GetLocalCurrency();
                        oDocument.Lines.CostingCode = oRecSet.Fields.Item("U_ProfitCtr").Value; ;

                        oDocument.Lines.UserFields.Fields.Item("U_B1S_PCTYPE").Value = "ITM";
                        oDocument.Lines.UserFields.Fields.Item("U_B1S_PCCODE").Value = oRecSet.Fields.Item("Code").Value;
                        oDocument.Lines.UserFields.Fields.Item("U_B1S_PCLINE").Value = oRecSet.Fields.Item("LineId").Value.ToString();
                        oDocument.Lines.UserFields.Fields.Item("U_B1S_Print").Value = oRecSet.Fields.Item("U_Print").Value;
                        
                        

                        oDocument.Lines.UserFields.Fields.Item("U_B1S_PCBASELIN").Value = lineid.ToString();
 
                    }
                    oRecSet.MoveNext();
                }

                j = oMatrix.RowCount;
                oMatrix.SetCellFocus(j, 1);
                

            }
            catch (Exception ex)
            {
                Global.SetMessage("Addlines. " + ex.Message, Global.MsgType.Error);
            }
        }

        private void AddVPOLines(string stDocEntry,string docentry, string NumAtCard, SAPbobsCOM.Documents oDocument)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbobsCOM.Recordset oRecSet2 = Global.Recordset();
                SAPbouiCOM.Matrix oMatrix;
                int j;

                oMatrix = coForm.Items.Item("38").Specific;

                oRecSet.DoQuery(Global.ChooseQuery(4, docentry));
                oRecSet.MoveFirst();

                oRecSet2.DoQuery(Global.ChooseQuery(13, stDocEntry));
                oRecSet2.MoveFirst();

                for (int i = 0; i < oRecSet.RecordCount; i++)
                {
                    j = oMatrix.RowCount;

                    if(ValidateRecord("VPO", stDocEntry, oRecSet.Fields.Item("DocEntry").Value.ToString(), oRecSet.Fields.Item("LineId").Value.ToString(), ""))
                    {                 
                        oDocument.Lines.Add();
                        oDocument.Lines.ItemCode = oRecSet.Fields.Item("U_PCItemCode").Value;
                        oDocument.Lines.ItemDescription = oRecSet.Fields.Item("U_PCItemDesc").Value;
                        oDocument.Lines.Quantity = Convert.ToDouble(oRecSet.Fields.Item("U_Sign").Value + "1");
                        oDocument.Lines.UnitPrice = oRecSet.Fields.Item("U_Amt").Value;
                        oDocument.Lines.Currency = Global.GetLocalCurrency();
                        oDocument.Lines.COGSCostingCode = oRecSet2.Fields.Item("U_ProfitCtr").Value;

                        oDocument.Lines.UserFields.Fields.Item("U_B1S_PCTYPE").Value = "VPO";
                        oDocument.Lines.UserFields.Fields.Item("U_B1S_VPOENTRY").Value = oRecSet.Fields.Item("DocEntry").Value;
                        oDocument.Lines.UserFields.Fields.Item("U_B1S_PCLINE").Value = oRecSet.Fields.Item("LineId").Value.ToString();
                        oDocument.Lines.UserFields.Fields.Item("U_B1S_Print").Value = "";

                    }
                    
                    oRecSet.MoveNext();
                }

                j = oMatrix.RowCount;
                oMatrix.SetCellFocus(j, 1);
            }
            catch (Exception ex)
            {
                Global.SetMessage("AddVPOLines. " + ex.Message, Global.MsgType.Error);
            }
        }


        private bool ValidateRecord(string type,string docentry ,string code, string line, string baseline)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                switch (type)
                {
                    case "ITM":
                        oRecSet.DoQuery(Global.ChooseQuery(5, docentry, baseline, code, line));
                        if (oRecSet.RecordCount > 0)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                      
                    case "VPO":
                        oRecSet.DoQuery(Global.ChooseQuery(6, docentry, code, line));
                        if (oRecSet.RecordCount > 0)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    default:
                        return false;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("ValidateRecord. " + ex.Message, Global.MsgType.Error);
                return false;
            }
        }

    }
}
