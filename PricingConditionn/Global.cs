﻿using SAPbobsCOM;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = SAPbouiCOM.Application;
using Company = SAPbobsCOM.Company;


namespace PricingConditionn
{
    public static class Global
    {


        public static Company oCompany;
        public static Application SBOApp;
        public static String csDirectory;
        public static bool flag = false;
        public enum MsgType
        {
            Error,
            Warning,
            Success
        }

        public static void SetMessage(String message, MsgType msgType)
        {
            switch (msgType)
            {
                case Global.MsgType.Error:
                    Global.SBOApp.StatusBar.SetSystemMessage(message, BoMessageTime.bmt_Short);
                    break;

                case Global.MsgType.Success:
                    Global.SBOApp.StatusBar.SetSystemMessage(message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    break;

                case Global.MsgType.Warning:
                    Global.SBOApp.StatusBar.SetSystemMessage(message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    break;

            }
        }

        public static SAPbobsCOM.Recordset Recordset()
        {
            try
            {
                Recordset oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                return oRecordset;
            }
            catch (Exception)
            {
                SetMessage("SetRecordSet", MsgType.Error);
                return null;
            }

        }
        
        public static string GetLocalCurrency()
        {
            try
            {
                Recordset oRecSet = Recordset();
                oRecSet.DoQuery("SELECT T0.MainCurncy FROM OADM T0 ");

                return oRecSet.Fields.Item(0).Value.ToString();
            }
            catch (Exception ex)
            {
                SetMessage("GetLocalCurrency " + ex.Message, MsgType.Error);
                return "";
            }
        }

        public static bool DisplayCurrencyRight()
        {
            try
            {
                Recordset oRecSet = Recordset();
                oRecSet.DoQuery("SELECT T0.CurOnRight  FROM OADM T0 ");

                if(oRecSet.Fields.Item(0).Value == "Y")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                SetMessage("DisplayCurrencyRight " + ex.Message, MsgType.Error);
                return true;
            }
        }

        public static void CreateForm(String csFormUID)
        {
            try
            {
                csFormUID = csFormUID + ".srf";
                LoadFromXML(ref csFormUID);
            }
            catch (Exception)
            {

            }
        }



        public static void LoadFromXML(ref string FileName)
        {

            System.Xml.XmlDocument oXmlDoc = null;

            oXmlDoc = new System.Xml.XmlDocument();

            string sPath = null;
            sPath = csDirectory;
            sPath = System.IO.Directory.GetParent(sPath).ToString();

            oXmlDoc.Load(sPath + "\\" + FileName);

            string sXML = oXmlDoc.InnerXml.ToString();
            Global.SBOApp.LoadBatchActions(ref sXML);

        }

        public static void AddMenu(String mainMenuId, BoMenuType menuType, string menuId, int position, string caption,
             string imgName)
        {
            SAPbouiCOM.Menus oMenus;
            SAPbouiCOM.MenuItem oMenuItem;

            oMenus = Global.SBOApp.Menus;

            if (oMenus.Exists(menuId))
            {
                return;
            }

            MenuCreationParams oCreationParams;
            oCreationParams = Global.SBOApp.CreateObject(BoCreatableObjectType.cot_MenuCreationParams);

            try
            {
                oMenuItem = Global.SBOApp.Menus.Item(mainMenuId);
                oMenus = oMenuItem.SubMenus;

                switch (menuType)
                {
                    case BoMenuType.mt_SEPERATOR:

                        oCreationParams.Type = BoMenuType.mt_SEPERATOR;
                        oCreationParams.UniqueID = menuId;
                        oCreationParams.Position = oMenuItem.SubMenus.Count + 1;
                        oCreationParams.Type = BoMenuType.mt_POPUP;
                        oCreationParams.String = caption;

                        if (imgName != "")
                        {
                            oCreationParams.Image = Global.csDirectory + imgName;
                        }

                        oMenus.AddEx(oCreationParams);

                        break;

                    case BoMenuType.mt_POPUP:
                        oCreationParams.Type = BoMenuType.mt_POPUP;
                        oCreationParams.UniqueID = menuId;
                        oCreationParams.String = caption;

                        if (position == -1)
                        {
                            oCreationParams.Position = oMenuItem.SubMenus.Count + 1;
                        }
                        else
                        {
                            oCreationParams.Position = position;
                        }

                        oMenus.AddEx(oCreationParams);

                        break;

                    case BoMenuType.mt_STRING:

                        oCreationParams.Type = BoMenuType.mt_STRING;
                        oCreationParams.UniqueID = menuId;
                        oCreationParams.String = caption;

                        if (position == -1)
                        {
                            oCreationParams.Position = oMenuItem.SubMenus.Count + 1;
                        }
                        else
                        {
                            oCreationParams.Position = position;
                        }


                        oMenus.AddEx(oCreationParams);
                        break;

                }

            }
            catch (Exception e)
            {
                Global.SetMessage("AddMenu. " + e.Message, Global.MsgType.Error);
            }
        }


        public static string ChooseQuery(int id, string param1 = "", string param2 = "", string param3 = "", string param4 = "")
        {
            switch (id)
            {
                case 1:
                  return "SELECT TOP 1 T0.Code, ISNULL(T0.U_EffDate,GETDATE()) FROM[@B1S_PC_HEADER] T0 " +
                         "WHERE T0.U_ItemCode = '" + param1 + "' AND(('" + param2 + "' BETWEEN T0.U_EffDate AND T0.U_ValidTill) OR(T0.U_EffDate is null AND T0.U_ValidTill is null)) " +
                         "ORDER BY ISNULL(T0.U_EffDate, GETDATE()) ASC, T0.Code DESC";
                case 2:
                    return "SELECT T0.Code, T0.LineId, T0.U_PCItemCode, T0.U_PCItemDesc, T0.U_Sign, T0.U_DiscAmt, T0.U_Print, T1.U_ProfitCtr " +
                           "FROM[@B1S_PC_ROW] T0 INNER JOIN [@B1S_PC_HEADER] T1 ON T0.Code = T1.Code  " +
                           "WHERE T0.Code = '" + param1 + "' AND T0.U_DiscAmt <> 0 AND(ISNULL(T0.U_DRGL, '') = '' AND ISNULL(T0.U_CRGL, '') = '')";
                case 3:
                    return "SELECT T0.DocEntry FROM [@B1S_PCVPO_H] T0 WHERE T0.U_VPO = '" + param1 + "' AND T0.Canceled = 'N' ";
                case 4:
                    return "SELECT T0.DocEntry, T0.LineId, T0.U_PCItemCode, T0.U_PCItemDesc, T0.U_Sign, T0.U_Amt " +
                           "FROM[@B1S_PCVPO_R] T0 " +
                           "WHERE T0.DocEntry = "+ param1 + " AND T0.U_Amt <> 0";
                case 5:
                    return "SELECT T1.DocEntry " +
                           "FROM RDR1 T1 " +
                           "WHERE T1.DocEntry = '" + param1 + "' AND T1.U_B1S_PCBASELIN = '" + param2 + "' AND T1.U_B1S_PCTYPE = 'ITM' AND T1.U_B1S_PCCODE = '" + param3 + "' AND T1.U_B1S_PCLINE = '" + param4 +"' ";
                case 6:

                    return "SELECT T1.DocEntry " +
                           "FROM RDR1 T1 " +
                           "WHERE T1.DocEntry = '" + param1 + "' AND T1.U_B1S_PCTYPE = 'VPO'  AND T1.U_B1S_VPOENTRY = '" + param2 + "' and T1.U_B1S_PCLINE = '" + param3 + "' ";
                case 7:

                    return "SELECT T0.DocNum,T1.ItemCode, T0.DocDate, T0.TaxDate,T5.DistNumber " +
                           "FROM INV1 T1 INNER JOIN OINV T0 ON T0.DocEntry = T1.DocEntry " +
                           "INNER JOIN OITM T2 ON T1.ItemCode = T2.ItemCode " +
                           "INNER JOIN OITB T3 ON T2.ItmsGrpCod = T3.ItmsGrpCod " +
                           "LEFT OUTER JOIN SRI1 T4 ON(CASE WHEN T1.BaseType = -1 THEN T1.ObjType WHEN T1.BaseType = '17' THEN T1.ObjType ELSE T1.BaseType END) = T4.BaseType  AND " +
                           "                          (CASE WHEN T1.BaseType = -1 THEN  T1.DocEntry WHEN T1.BaseType = '17' THEN T1.DocEntry ELSE T1.BaseEntry END) = T4.BaseEntry AND " +
                           "                          (CASE WHEN T1.BaseType = -1 THEN  T1.LineNum  WHEN T1.BaseType = '17' THEN T1.LineNum  ELSE T1.BaseLine END) = T4.BaseLinNum AND T1.ItemCode = T4.ItemCode AND T4.Direction = 1 " +
                           "LEFT OUTER JOIN OSRN T5 ON T4.SysSerial = T5.SysNumber AND T5.ItemCode = T1.ItemCode " +
                           "WHERE T1.DocEntry = '" + param1 +"' AND ISNULL(T1.U_B1S_PCTYPE,'') = '' AND T3.U_B1S_Vehicle = 'YES'";

                case 8:
                    return "SELECT T0.Code, T0.LineId, T0.U_PCItemCode, T0.U_PCItemDesc, T0.U_Sign, T0.U_Amt, T0.U_Print, T1.U_ProfitCtr, T2.AcctCode [U_DRGL], T3.AcctCode [U_CRGL] " +
                           "FROM[@B1S_PC_ROW] T0 INNER JOIN[@B1S_PC_HEADER] T1 ON T0.Code = T1.Code " +
                                                "INNER JOIN OACT T2 ON T0.U_DRGL = T2.FormatCode " +
                                                "INNER JOIN OACT T3 ON T0.U_CRGL = T3.FormatCode " +
                           "WHERE T0.Code = '" + param1 + "' AND T0.U_Amt <> 0 AND ISNULL(T0.U_DRGL, '') != '' AND ISNULL(T0.U_CRGL, '') != ''";

                case 9:

                    return "SELECT T0.DocEntry " +
                           "FROM OINV T0 INNER JOIN INV1 T1 ON T0.DocEntry = T1.DocEntry " +
                           "INNER JOIN[@B1S_PC_HEADER] T2 ON T1.ItemCOde = T2.U_ItemCode AND((T0.DocDate BETWEEN T2.U_EffDate AND T2.U_ValidTill) OR(T2.U_EffDate is null AND T2.U_ValidTill is null))  " +
                           "WHERE T0.DocEntry = '" + param1 + "'";

                case 10:
                    return " DECLARE @DistNumber AS VARCHAR(254) " +
                            "DECLARE @ItemCode AS VARCHAR(254) " +
                            "DECLARE @LastInvEntry AS VARCHAR(50) " +
                            "CREATE TABLE #InvoiceJE (TransId VARCHAR(100)) " +
                            "                DECLARE cursorBase CURSOR " +
                            "               FOR " +
                            "               SELECT T5.DistNumber, T5.ItemCode " +
                            "                FROM ORIN T0 INNER JOIN RIN1 T1 ON T0.DocEntry = T1.DocEntry " +
                            "                            INNER JOIN OITM T2 ON T1.ItemCode = T2.ItemCode " +
                            "                                INNER JOIN OITB T3 ON T2.ItmsGrpCod = T3.ItmsGrpCod " +
                            "                            INNER JOIN SRI1 T4 ON(CASE WHEN T1.BaseType = 13 THEN  '14' ELSE '14' END) = T4.BaseType  AND " +
                            "                            (CASE WHEN T1.BaseType = 13 THEN  T1.DocEntry ELSE T1.DocEntry END) = T4.BaseEntry AND " +
                            "                            (CASE WHEN T1.BaseType = 13 THEN  T1.LineNum ELSE T1.LineNum END) = T4.BaseLinNum AND T1.ItemCode = T4.ItemCode " +
                            "                            INNER JOIN OSRN T5 ON T4.SysSerial = T5.SysNumber AND T5.ItemCode = T1.ItemCode " +
                            "                WHERE T0.DocEntry = '" + param1 + "' AND ISNULL(T1.U_B1S_PCTYPE,'') = '' AND T3.U_B1S_Vehicle = 'YES' " +
                            "               OPEN cursorBase FETCH cursorBase INTO @DistNumber, @ItemCode " +
                            "                WHILE(@@FETCH_STATUS = 0) " +
                            "                BEGIN " +
                            "                            SELECT TOP 1 @LastInvEntry = T0.DocEntry " +
                            "                            FROM INV1 T1 INNER JOIN OINV T0 ON T0.DocEntry = T1.DocEntry " +
                            "                            INNER JOIN OITM T2 ON T1.ItemCode = T2.ItemCode " +
                            "                            INNER JOIN OITB T3 ON T2.ItmsGrpCod = T3.ItmsGrpCod " +
                            "                           INNER JOIN SRI1 T4 ON(CASE WHEN T1.BaseType = -1 THEN T1.ObjType WHEN T1.BaseType = '17' THEN T1.ObjType ELSE T1.BaseType END) = T4.BaseType  AND " +
                            "                                                   (CASE WHEN T1.BaseType = -1 THEN  T1.DocEntry WHEN T1.BaseType = '17' THEN T1.DocEntry ELSE T1.BaseEntry END) = T4.BaseEntry AND " +
                            "                                                   (CASE WHEN T1.BaseType = -1 THEN  T1.LineNum  WHEN T1.BaseType = '17' THEN T1.LineNum  ELSE T1.BaseLine END) = T4.BaseLinNum AND T1.ItemCode = T4.ItemCode AND T4.Direction = 1 " +
                            "                            LEFT OUTER JOIN OSRN T5 ON T4.SysSerial = T5.SysNumber AND T5.ItemCode = T1.ItemCode " +
                            "                            WHERE ISNULL(T1.U_B1S_PCTYPE,'') = '' AND T3.U_B1S_Vehicle = 'YES'  AND T5.DistNumber = @DistNumber   AND T5.ItemCode = @ItemCode " +
                            "                            ORDER BY T0.DocDate desc, T0.DocNum desc " +
                            "                           INSERT #InvoiceJE " +
                            "						    SELECT TOP 1  T0.TransId " +
                            "                           FROM OJDT T0 " +
                            "                            WHERE T0.Ref1 = @DistNumber AND T0.Ref2 = @ItemCode AND T0.U_B1S_PCBASETYPE = '13' AND T0.Ref3 = @LastInvEntry " +
                            "                           Order By T0.RefDate desc, T0.TransId desc " +
                            "                FETCH cursorBase INTO @DistNumber, @ItemCode " +
                            "                END " +
                            "               CLOSE cursorBase " +
                            "               DEALLOCATE cursorBase " +
                            "    SELECT * FROM #InvoiceJE" ;
                case 11:
                    return "SELECT T0.Ref1, T0.Ref2, T1.Account, T1.Credit, T1.Debit, T1.U_B1S_PCCODE, T1.U_B1S_PCTYPE, T1.U_B1S_PCLINE, T1.ProfitCode " +
                            "FROM OJDT T0 INNER JOIN JDT1 T1 on T0.TransId = T1.TransId " +
                            "WHERE T0.TransId = '" + param1 + "'";
                case 12:
                    return "SELECT TOP 1 T0.DocDate, T0.DocDueDate, T0.TaxDate, T0.DocNum, CASE WHEN ISNULL(T1.BaseRef,'')  = '' THEN T0.U_B1S_InvNo ELSE T1.BaseRef END [Reference3] FROM ORIN T0 INNER JOIN RIN1 T1 ON T0.Docentry = T1.DocEntry WHERE T0.DocEntry = '" + param1 + "'";

                case 13:
                    return "DECLARE @ItemCode AS VARCHAR(100) " +
                            "DECLARE @DocDate AS DATE " +
                            "SELECT TOP 1 @ItemCode = T0.ItemCode, @DocDate = T1.DocDate " +
                            "FROM RDR1 T0 INNER JOIN ORDR T1 ON  T0.DocEntry = T1.DocEntry " +
                            "WHERE T0.DocEntry = '" + param1 + "' AND ISNULL(T0.U_B1S_PCTYPE,'') = '' " +
                            "SELECT TOP 1 T0.U_ProfitCtr FROM[@B1S_PC_HEADER] T0 " +
                            "WHERE T0.U_ItemCode = @ItemCode AND((@DocDate BETWEEN T0.U_EffDate AND T0.U_ValidTill) OR(T0.U_EffDate is null AND T0.U_ValidTill is null))   " +
                            "                         ORDER BY ISNULL(T0.U_EffDate, GETDATE()) ASC, T0.Code DESC";

                default:
                    return "";

            }
        }

        public static void SaveLog(string Log)
        {
            System.IO.StreamWriter swFile;
            try
            {
                swFile = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt", true, System.Text.Encoding.Default);
                swFile.WriteLine(Log + " " + DateTime.Now.Date.ToShortDateString() + " " + DateTime.Now.TimeOfDay.ToString());
                swFile.Close();
            }
            catch (Exception)
            {
            }

        }
    }
}
